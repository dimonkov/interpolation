﻿using System;
using System.Collections.Generic;

namespace Interpolation
{
	public class LagrangeInterpolation
	{
		public static double CalculateInterpolatedValue(List<Point> points, double x, int order = 1)
		{
			if (order >= points.Count || order <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(order), order, $"Value of {nameof(order)}" +
							$" must be less than point count and greater than zero.{Environment.NewLine}" +
							$"Expected: <{points.Count}, got: {order}");
			}

			double ans = 0.0d;
			for (int i = 0; i <= order; i++)
			{
				ans += points[i].y * CalculateC(points, i, x, order);
			}

			return ans;
		}

		private static double CalculateC(List<Point> points, int index, double x, int order)
		{
			var top = CalculateProd(points, x, order, index);

			var bottom = CalculateProd(points, points[index].x, order, index);

			var val = top / bottom;

			return val;
		}

		private static double CalculateProd(List<Point> points, double x, int order, int xIdex)
		{
			double prod = 1.0d;

			for (int i = 0; i <= order; i++)
			{
				if (i == xIdex)
					continue;

				prod *= x - points[i].x;
			}

			return prod;
		}
	}
}
