﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Interpolation
{
    public class NewtonsInterpolation
    {
        public static DiffTable GetDiffTable(List<Point> points, int order)
        {
            var pointCount = points.Count;

			if (order >= pointCount || order <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(order), order, $"Value of {nameof(order)}" +
							$" must be less than point count and greater than zero.{Environment.NewLine}" +
							$"Expected: <{pointCount}, got: {order}"); 
			}

			var x = points.Select(p => p.x).ToList();

            var table = InitOrderTable(points, order);

            for (int ord = 1; ord <= order; ord++)
            {
                for (int i = ord ; i < pointCount; i++)
                {
					//The previous order, e.g. if calculating 2-nd order,
					//take values calculated for the 1-st order
					var prevOrd = table[ord - 1]; 

					//Calculate the top part, ord is substracted from i because each order has less values
                    var top = (prevOrd[i - ord] - prevOrd[i - ord + 1]);

					//Calculate the bottom part, ord is substracted from i because each order has less values
					var bottom = (x[i - ord] - x[i]);
                    table[ord][i - ord] = top / bottom; 
                }   
            }

            return new DiffTable(x, table);
        }

        private static List<List<double>> InitOrderTable(List<Point> points, int totalOrders)
        {
            var pointCount = points.Count;

			//We need more space than orders we have,
			//because will be storing Y values as order 0

            var table = new List<List<double>>(totalOrders + 1);

            for (int i = 0; i <= totalOrders; i++)
            {
                table.Add( new List<double>(new double[pointCount - i]));
            }

			//Fill in Y values as order 0
            for (int i = 0; i < pointCount; i++)
            {
                table[0][i] = points[i].y;
            }

            return table;
        }
    }

    public class DiffTable
    {
        public int TableOrder { get; protected set; }

        private List<List<double>> _table;

        private List<double> _x;

        public DiffTable(List<double> x, List<List<double>> diff)
        {
			if(x.Count <= 1)
			{
				throw new ArgumentException("Point count is less than or equal to 1, nothing to interpolate between.", nameof(x));
			}

			if(x.Count != diff[0].Count)
			{
				throw new ArgumentOutOfRangeException($"Both {nameof(x)} and {nameof(diff)}[0] should have the same length");
			}

            _x = x;

            _table = diff;

            TableOrder = diff.Count - 1;
        }

        public double CalculateInterpolatedValue(double x, int index = 0, int order = 1)
        {
			if(order > TableOrder || order <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(order), order, $"Value of {nameof(order)}" +
					$" must be less or equal to the table's order and greater than zero.{Environment.NewLine}" +
					$"Expected: <={TableOrder}, got: {order}");
			}

			if(_x.Count - order <= index || index < 0)
			{
				throw new ArgumentOutOfRangeException(nameof(index), index, $"Value of {nameof(index)}" +
					$" must be less than point count - {nameof(order)}" +
					$" and greater or equal to zero.{Environment.NewLine}" +
					$"Expected: <={_x.Count - order - 1}, got: {index}");
			}

            var yVal = _table[0][index]; //Take the y[index] value
            var xVal = x - _x[index]; //Init with x - x[index]

			yVal += xVal * _table[1][index]; //Multiply x by f[x0,x1]

            for (int ord = 2; ord <= order; ord++)
            {
                xVal *= x - _x[ord-1 + index];
                yVal += _table[ord][index] * xVal;
            }

            return yVal;
        }
    }
}
