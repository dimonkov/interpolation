﻿namespace Interpolation
{
    public struct Point
    {
        public double x;
        public double y;
    }
}
