﻿using System;
using System.Collections.Generic;

namespace Interpolation
{
	class Program
    {
        static void Main(string[] args)
        {
            var points = new List<Point>
            {
                new Point { x = 1, y = 0.230 },
                new Point { x = 3, y = 0.228 },
                new Point { x = 4, y = 0.226 },
                new Point { x = 5, y = 0.222 },
                new Point { x = 7, y = 0.223 },
                new Point { x = 8, y = 0.218 },
                new Point { x = 9, y = 0.206 },
                new Point { x = 10, y = 0.202 },
            };

			int order = points.Count - 1;
			double xValue = 2;

            double trueValue = 0.231;

            var dTable = NewtonsInterpolation.GetDiffTable(points, order);

			double prev = 0.0d;
            for (int i = 1; i <= order; i++)
            {
                var newtons = dTable.CalculateInterpolatedValue(xValue, order: i);
				var rnx = Math.Abs(i > 1 ? newtons - prev : 0.0);
				var err = Math.Abs(trueValue - newtons);
				prev = newtons;

				var firstColumn = $"Newtons {i} order: {newtons.ToString("0.####")}".PadBoth(40);
				var secondColumn = $"|Y-Ln(x)|: {err.ToString("0.####")}".PadBoth(25);
				var thirdColumn = $"|Rn(x)|: {rnx.ToString("0.####")}".PadBoth(25);
				var fourthColumn = $"Ln(x) + Rn(x): {(newtons + rnx).ToString("0.####")}".PadBoth(25);

				Console.WriteLine($"{firstColumn}{secondColumn}{thirdColumn}{fourthColumn}");
            }
        }
    }
}
